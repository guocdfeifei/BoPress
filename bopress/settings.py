# -*- coding: utf-8 -*-
__author__ = 'yezang'

# 调试
DEBUG = True
# 服务器启动立即打开浏览器
USE_BROWSER = True
# 域名, 不需要以``/``结尾
DOMAIN = "http://127.0.0.1:9090"
# 超级管理员 ``登录名``
ADMIN_USER_LOGIN = "super"
# 超级管理员 ``邮箱地址``
ADMIN_USER_EMAIL = "super@youdomain.com"
# 日志级别
LOGGER_LEVEL = None
# 应用根目录
BASE_DIR = ""
# 插件目录
PLUGINS_ROOT = ""
# Cookie 安全 tornado
COOKIE_SECRET = "xxxxxx"
# session 超时(秒)
SESSION_TIMEOUT = 300
# 最大session存储数目，超过则会删除其中一些.
SESSION_STORE_MAX_ITEMS = 10000
# xsrf 保护
XSRF_COOKIES = True
# 缓存方式 `simple` `file`, `mem`, `redis`
CACHE_MODE = "simple"
# 数据表前缀
TABLE_NAME_PREFIX = "bo_"
# func for create engine
DBEngine = None
