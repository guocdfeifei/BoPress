function bo_alert(title, msg, icon) {
    if (msg === undefined || msg.length === 0 || $.isNumeric(msg)) {
        return;
    }
    $.toast({
        text: msg,
        heading: title,
        icon: icon,
        showHideTransition: 'fade',
        allowToastClose: false,
        hideAfter: 3000,
        stack: false,
        position: 'mid-center',
        textAlign: 'left',
        loader: false,
        loaderBg: '#ffffff'
    });
}

function bo_alert_success(msg, title) {
    if (title !== undefined && title.length > 0) {
        bo_alert(title, msg, 'success');
    } else {
        bo_alert(false, msg, 'success');
    }
}

function bo_alert_error(msg, title) {
    if (title !== undefined && title.length > 0) {
        bo_alert(title, msg, 'error');
    } else {
        bo_alert(false, msg, 'error');
    }
}

function bo_alert_info(msg, title) {
    if (title !== undefined && title.length > 0) {
        bo_alert(title, msg, 'info');
    } else {
        bo_alert(false, msg, 'info');
    }
}

function bo_alert_warning(msg, title) {
    if (title !== undefined && title.length > 0) {
        bo_alert(title, msg, 'warning');
    } else {
        bo_alert(false, msg, 'warning');
    }
}

function bo_valid_errors(form_id, errors, message) {
    var form = $("#" + form_id);
    form.find(".form-group").removeClass("has-error");
    form.find(".help-block").remove();
    var elms = {};
    var form_controls = form.find(".form-control");
    form_controls.each(function (k, v) {
        var elem_name = $(this).attr("name");
        if (elem_name !== undefined && $.trim(elem_name) !== "") {
            elms[elem_name] = v;
        }
    });
    var has_err = false;
    for (var key in errors) {
        has_err = true;
        if (!elms.hasOwnProperty(key)){
            continue;
        }
        var parent = $(elms[key]).parent();
        parent.addClass("has-error");
        var tip = '<span class="help-block">' + errors[key] + '</span>';
        parent.append(tip);
    }
    if (!has_err && message.length !== 0) {
        bo_alert_error(message);
    }
}